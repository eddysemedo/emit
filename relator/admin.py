from django.contrib import admin
from relator.models import *
# Register your models here.

admin.site.register(Relatorio)
admin.site.register([Grafico, Caixa_de_Texto, Tabela])

# Modelos, Imagens