from django.db import models

# Create your models here.

class Relatorio(models.Model):
	titulo_relatorio = models.CharField(max_length = 20)

	def __str__(self):
		return self.titulo_relatorio

# clas Modelos(models.Model):
# 	titulo_relatorio = models.CharField(max_length = 100)

class Grafico(models.Model):
	titulo_grafico = models.CharField(max_length = 100, primary_key=True)
	relatorio_linkado = models.ForeignKey(Relatorio)

	def __str__(self):
		return self.titulo_grafico

class Tag(models.Model):
	tag_ref = models.ForeignKey(Grafico)
	nome_tag = models.CharField(max_length = 30)

class TipoGrafico(models.Model):
	tp_grafico_linkado = models.ForeignKey(Grafico)

class Entrada(models.Model):
	entrada_tp_grafico = models.ForeignKey(TipoGrafico)
	entrada = models.IntegerField()

class Caixa_de_Texto(models.Model):
	relatorio_linkado = models.ForeignKey(Relatorio)
	texto = models.TextField()

class Tabela(models.Model):
	relatorio_linkado = models.ForeignKey(Relatorio)
	linhas = models.IntegerField()
	colunas = models.IntegerField()

class Linha(models.Model):
	linha = models.CharField(max_length = 30)
	
