# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relator', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='grafico',
            name='id',
        ),
        migrations.AlterField(
            model_name='grafico',
            name='titulo_grafico',
            field=models.CharField(primary_key=True, max_length=100, serialize=False),
        ),
    ]
