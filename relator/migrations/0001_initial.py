# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Caixa_de_Texto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('texto', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Entrada',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('entrada', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Grafico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('titulo_grafico', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Relatorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('titulo_relatorio', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Tabela',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('linhas', models.IntegerField()),
                ('colunas', models.IntegerField()),
                ('relatorio_linkado', models.ForeignKey(to='relator.Relatorio')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('nome_tag', models.CharField(max_length=30)),
                ('tag_ref', models.ForeignKey(to='relator.Grafico')),
            ],
        ),
        migrations.CreateModel(
            name='TipoGrafico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('tp_grafico_linkado', models.ForeignKey(to='relator.Grafico')),
            ],
        ),
        migrations.AddField(
            model_name='grafico',
            name='relatorio_linkado',
            field=models.ForeignKey(to='relator.Relatorio'),
        ),
        migrations.AddField(
            model_name='entrada',
            name='entrada_tp_grafico',
            field=models.ForeignKey(to='relator.TipoGrafico'),
        ),
        migrations.AddField(
            model_name='caixa_de_texto',
            name='relatorio_linkado',
            field=models.ForeignKey(to='relator.Relatorio'),
        ),
    ]
