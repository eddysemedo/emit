# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relator', '0002_auto_20150603_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relatorio',
            name='titulo_relatorio',
            field=models.CharField(max_length=20),
        ),
    ]
