from django.db import models

# Create your models here.

class Membro(models.Model):
	primeiro_nome = models.CharField(max_length=30)
	ultimo_nome = models.CharField(max_length=30)
	email = models.EmailField()
	senha = models.CharField(max_length=30)

class Administrador(models.Model):
	admin = models.ForeignKey(Membro)

class UsuariosComum(models.Model):
	usuariocomum = models.ForeignKey(Membro)


class Tipos_Permissao_Adm(models.Model):
	tp_ref = models.ForeignKey(Membro)
	tipo_permissao = models.CharField(max_length=50)
