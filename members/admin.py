from django.contrib import admin
from members.models import *

# Register your models here.
admin.site.register([Membro, UsuariosComum, Administrador])
