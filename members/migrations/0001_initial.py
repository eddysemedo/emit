# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Membro',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('primeiro_nome', models.CharField(max_length=30)),
                ('ultimo_nome', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=254)),
                ('senha', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Tipos_Permissao_Adm',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('tipo_permissao', models.CharField(max_length=50)),
                ('tp_ref', models.ForeignKey(to='members.Membro')),
            ],
        ),
        migrations.CreateModel(
            name='UsuariosComum',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('usuariocomum', models.ForeignKey(to='members.Membro')),
            ],
        ),
        migrations.AddField(
            model_name='administrador',
            name='admin',
            field=models.ForeignKey(to='members.Membro'),
        ),
    ]
